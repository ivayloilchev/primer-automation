# FE automation testing project skeleton

This (java/gradle) project represents generic `skeleton` application which can be used as basis for almost any front end test automation project solution.

## Solution attributes

**Primary attributes** of Functional (End-2-End / UI / System) Test Automation solution:

1. The test should be specified only once. The implementation details should figure out how to get the execution happening for supported platforms.
  
  `1Link to user story in JIRA, link to use case in JIRA, Feature file in solution where requirements are missing`
  
2. Ability to run the full suite or a subset of tests on demand.
  
  `1External configuration in xml file + use of attributes`
  
3. Ability to run tests across any environment.
  
  `1Environment configuration in property file`
  
4. Ability to run tests on supported browsers regardless of their version.
  
  `1Using WebDriverManager and configuration`
  
5. Ability to easily specify test data for each supported environment.
  
  `1Test data in external file (xls, json, txt) or embedded in test classes where feasible`
  
6. Rich reporting
  
  `1All test execution should be reported using well formatted execution metrics`
  
7. Documentation comments (Javadoc) for classes and methods.
  
  `1All code (classes, methods and important variables and constants) should have clear and concise comment`
  

Additional attributes

1. Support for parallel / distributed execution of tests
  
2. Ability to run on local machines or in the CI
  
3. Implemented test should be robust, efficient and fast
  

## Basic building blocks of the solution

Basic building blocks on which solution is based upon:

- Common structure and directory layout for the project base on `Standard Directory Layout for maven project(s)`
- Project management/build tool [`maven` or `gradle`]
- Unit testing framework (`TestNG`) for managing and running tests
- Selenium WebDriver as core technology for front-end automation solution
- Common test automation design patterns (`POM` and `Page Factory`) for code organization
- Reporting
- Logging

### Automation project common structure

Standard Directory Layout for **maven project** wil be used, with initial structure:

```markdown
project-home
  └── project-name                    // (sub)project name
        └── src
             ├── main
             |    ├── java         // Application/Library sources
             |    └── resources    // Application/Library resources
             └── test
                  ├── java            // Test sources
                  └── resources       // Test resources
```

### Project management/build tool

For **gradle** based project, the structure is:

```java
project-home
  ├── gradle           // generated folder for wrapper files
  |     ├── wrapper
  ├── gradlew          // gradle wrapper start script
  ├── gradlew.bat      // gradle wrapper start script
  ├── setting.gradle   // settings file for build name (& subprojects)    
  └── project-name                    // (sub)project name
        └─── src
              ├─── main
              |       ├─── java                   // Application sources
              |       |     └─── organization
              |       |           ├─── config     // Configuration file
              |       |           ├─── driver     // Handling driver to manage browsers
              |       |           ├─── model      // Model classes
              |       |           ├─── pages      // POM (Project Object Model) pages
              |       |           └─── utilities  // Various utility classes
              |       └─── resources              // Application resources
              └─── test
                    ├── java                      // Test sources (test classes)
                    |    └─── organization
                    |          └─── `TestBase.java`
                    └─── resources                // Test resources
                           ├─── configuration.properties    // configuration settings
                           └─── testng.xml                  // TestNG RUN configuration 
```

For **maven** based project, the structure is:

```java
project-home
  ├──pom.xml          // configuration information about the project
  └── src
       ├── main
       |    ├── java          // Application sources + sub-folders as in gradle project
       |    └── resources     // Application resources + sub-folders as in gradle project
       └── test
            ├── java          // Test sources + sub-folders as in gradle project
            └── resources     // Test resources + sub-folders as in gradle project
```

### Unit testing framework (`TestNG`) for managing and running tests

`TestNG` is an open source automated testing framework (NG means **NextGeneration**) inspired from JUnit and NUnit,
but introducing some new functionalities that make it more powerful and easier to use.

Most common usage of TestNG on the projects:

- Organizing and managing test classes in the project
- Test run configuration
- Simple out-of-the-box reporting about test run

### Selenium WebDriver

Selenium is a **browser automation tool**, commonly used for writing end-to-end tests of web applications.
A browser automation tool does exactly what you would expect: automate the control of a browser so that repetitive tasks can be automated.

`Selenium WebDriver` is a free and open source library for automated testing of web applications.
WebDriver uses browser automation APIs provided by browser vendors to control browser and run tests.

`Selenium WebDriver` is core technology that will be used for authoring and running test for **Front End** applications solutions!!!

### Common test automation design patterns

The `Page Object Pattern` **wraps** all *elements*, *actions* and *validations* happening on a page in one single object- Page Object.

The `PageFactory` provides a convenient way of **initializing** and **mapping** the Page Object fields.
Page Factory is an extension to page objects that are used to **initialize the web elements** that are defined on the page object & instantiate the Page Objects itself.

## Naming conventions & Guidelines

### Conventions

For standard naming conventions, see [9 - Naming Conventions](https://www.oracle.com/java/technologies/javase/codeconventions-namingconventions.html "https://www.oracle.com/java/technologies/javase/codeconventions-namingconventions.html")

For example:

1. **Packages**: all-lowercase top-level domain followed by project name: `com.e2e`
  
2. **Class names** should be nouns, in mixed case with the first letter of each internal word capitalized: `class LoginPage`
  
3. **Interface** names should be capitalized like class names: `Configuration`
  
4. **Methods** should be verbs, in mixed case with the first letter lowercase, with the first letter of each internal word capitalized: `login()`, `enterUsername()`, `isLoginFinished()`…
  
5. **Variables** are in mixed case with a lowercase first letter. Internal words start with capital letters: `WebDriver driver;`
  
6. **Constants** should be all uppercase with words separated by underscores ("_"): `static final int MIN_WIDTH = 4;`, `static final String FOOTER_TEXT = "footer";`,…
  
7. **WebElement** starts with prefix `loc_` followed by `<elementType>` and `<elementName>`:
  

|     |     |     |
| --- | --- | --- |
| **WebElement Type** | Prefix | Example |

|     |     |     |
| --- | --- | --- |
| **WebElement Type** | Prefix | Example |
| TextBox | txt | loc_txtEmail, loc_txtPassword |
| Button | btn | loc_btnRegister, loc_btnLogin |
| CheckBox | cb  | loc_cbGender, loc_cbSalaryRange |
| DropDown | dd  | loc_ddCountry, loc_ddYear |
| Hyperlink | lnk | loc_lnkFQA |
| Image | img | loc_imgLogo |
| Label | lbl | loc_lblUserName |
| ListBox | lb  | loc_lbPolicyCodes |
| RadioButton | rbtn | loc_rbtnIsMandatory |
| Table | tbl | loc_tblBooks, loc_tblProducts |
| Header | hrd | loc_hdrPrint, loc_hdrUser |


### Guidelines

- FE Test Automation project should be simple (**KISS**) but not to simple!
  
- `Agile` lightweight approach to the documentation should be used
  
  - `Working software over comprehensive documentation`
    
  - Keep documentation *just simple enough* (the simplest that gets the job done), but not too simple
    
  - Write the fewest documents (travel as light as you possibly can): User stories, use cases, features files
    
  - Put the information in the most appropriate place (**Single Source of Information** aka JIRA/Confluence pages)
    
- Use comments everywhere (Javadoc style)
  
- Always implement things when you actually need them. "You aren't gonna need it" (**YAGNI**) states that programmer should not add functionality until deemed necessary.
  
- Follow **Reused Abstractions Principle**. *Having only one implementation of a given interface is a code smell!*
  
- Avoid using design patterns for sake of design patterns, use it only when needed.
  

### Reporting

Good test execution reports are essential to understanding the state of the product-under-test. The reports should have lot of information about the executed tests:

- screenshots
- server logs
- meta-data related to the test execution (CI build-number, browser / device, OS version...)

TestNG provides simple reporting mechanism (out of the box). Only step necessary is to enable it:

> In IDEA, go to `Run | Edit Configuration | TestNG | <YOUR_SUITE> | Listeners tab` and check `Use default reporter`!

TestNG comes with certain predefined listeners and by default they are added to the test execution. Whenever TestNG is run, HTML and XML reports are generated by default in the directory `./test-output` or `build/test-results/test`.

In order to use this predefined reporting system, file like `testng.xml` (or `smoketest.xml`...) should be added in `src/test/resources` folder. To modify run configuration:

1. Right click on `testng.xml` file and select `Modify Run Configuration...`
  
2. Enter `<PROJ_NAME>TestSuite` for the name of configuration
  
3. Select `Suite` as **Test kind**
  
4. In the **Suite** input, enter path to the above xml configuration file
  
5. Click **Apply** button
  

xml` configuration file example:

```xml
<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE suite SYSTEM "http://testng.org/testng-1.0.dtd"><suite name="SmokeSuite">    <test name="SmokeTests" preserve-order="true">        <classes>            <class name="com.organization.feature1.usecase1"/>            <class name="com.organization.feature1.usecase2"/>            <class name="com.organization.feature2.usecase1"/>            <!-- ... -->
        </classes>    </test></suite>
```

Then , select `Run|Edit Configuration...|TestNG`...

1. On `Configuration` tab, select `Listeners` tab.
  
2. Click on plus sign and from list of available listeners, select `...TestHTMLReporter`!
  
3. Click `Apply` button.
  

When test run is done, you'll find reports in `./test-output/<YOUR_SUITE_NAME>` folder. Just open `<test name>.html` file in your browser of choice...

In order to show report **automatically**, after test run, implement the following changes:

1. Congifuration info in `configuration.properties` file:

```ini
showReport = true
```

2. Changes to `TestBase.java` class:

```java
private String urlReport = System.getProperty("user.dir") + "\\test-output\\SmokeSuite\\" + "SmokeTests.html";
boolean autoShowReport = ConfigurationManager.configuration().showReport();    @AfterSuite(alwaysRun = true)    public void suiteTearDown(){        if(driver!=null){ driver.quit(); }        if(autoShowReport){            File htmlFile = new File(urlReport);            try {                Desktop.getDesktop().browse(htmlFile.toURI());            } catch (IOException e) {                e.printStackTrace();            }        }    }
```

### Logging

[Logback](https://logback.qos.ch/) is one of the most widely used logging frameworks in the Java Community.
It's a [replacement for its predecessor, Log4j.](https://logback.qos.ch/reasonsToSwitch.html)
Logback offers a faster implementation, provides more options for configuration, and more flexibility, hence we'll be used in our project!

Most common **pattern of usage** is:

```java
public class ClassName {

    private static final Logger logger 
      = LoggerFactory.getLogger(ClassName.class);

    public void someMethod(String parameter) {
        try{
            // Some code here
            logger.debug("Logging debug info when required");
        } catch (Exception e){
            logger.error("Logging error: " + e.getMessage());
        }
    }
}
```

Logback delegates the task of writing a logging event to components called **appenders**. In order to see any logging from application, at least one logging appender must be defined. File named `logback.xml` should be created in folder `src/resources/` with the following content (see example bellow) in order to see log in console window!

## Running tests

### Runnung individual tests

In order to run individual (class of) test, contex(right) click on desired test class and select `Run <TEST_CLASS_NAME>` option:

![](images/run-single-test.png)

<?xml version="1.0" encoding="UTF-8"?>

<configuration>
    <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
        <encoder>
            <pattern>%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n</pattern>
        </encoder>
    </appender>
    <root level="DEBUG">
        <appender-ref ref="STDOUT" />
    </root>
    <root level="ERROR">
        <appender-ref ref="STDOUT"/>
    </root>
</configuration>

### Running the suite

In order to run all the tests in the suite, contex(right) click on configuration file and select `Run <CONFIG_FILE_NAME.xml>` option:

![](images/run-test-suite.png)

## 🔗 Links

| ID  | Desc | Link |
| --- | --- | --- |
| 1   | Gradle User Manual | [Gradle User Manual](https://docs.gradle.org/current/userguide/userguide.html) |
| 2   | TestNG | [TestNG - Welcome](https://testng.org/doc/) |
| 3   | OWNER | [http://owner.aeonbits.org/](http://owner.aeonbits.org/) |
| 4   | Logback | [https://logback.qos.ch/](https://logback.qos.ch/) |
| 5   | A Guide To Logback | [A Guide To Logback \\| Baeldung](https://www.baeldung.com/logback) |
| 6   | Extent Reports in Selenium with TestNG | [Extent Reports in Selenium with TestNG](https://www.swtestacademy.com/extent-reports-in-selenium-with-testng/) |
| 7   | Custom Reporting with TestNG | [Custom Reporting with TestNG \\| Baeldung](https://www.baeldung.com/testng-custom-reporting) |
| 8   | TestNG Report Example | [TestNG Report Example](https://www.dev2qa.com/testng-report-example/) |
| 9   | Configuring ReportNG with TestNG for HTML Reports |     |
| 10  | Selenium Java Design Patterns - WebDriver Factory Pattern | https://www.linkedin.com/pulse/selenium-java-design-patterns-webdriver-factory-kushan-shalindra |

The project that this solution is for is Primer, Internal HR system which helps the employees for Time Off,
Worklogs, Sick Leave and other essential stuff for every company.

The purpose of the solution is to automate the FE of Primer, so it can be run for regression testing and save
time of the QA engineers.

