package com.e2e.pages.authentication;

import com.e2e.model.Student;
import com.e2e.pages.main.HomePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class LoginPagePrimer {


    WebDriver driver;
    int waitTime = 0;
    public static boolean isUserLogged = false;

    @FindBy(id = "Username")
    private WebElement loc_inputUsername;

    @FindBy(id = "Password")
    private WebElement loc_inputPassword;

    @FindBy(id = "loginBtn")
    private WebElement loc_btnLogin;


    public LoginPagePrimer(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, waitTime), this);
    }

    public void setUsername(String username) {
        loc_inputUsername.sendKeys(username);
    }

    public void setPassword(String password) {
        loc_inputPassword.sendKeys(password);
    }

    public void clkLoginButton() {
        loc_btnLogin.click();

    }

    public HomePage setFormAndLogin(String username, String password) {
        setUsername(username);
        setPassword(password);
        clkLoginButton();
        return new HomePage(driver);

    }

    public void setInvalidFormAndLogin(String username, String password) {
        setUsername(username);
        setPassword(password);
        clkLoginButton();

    }

    public WebElement getLoc_inputUsername() {
        return loc_inputUsername;
    }




}
