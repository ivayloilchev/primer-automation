package com.e2e.pages.authentication;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class SecretQuestionPage {

    WebDriver driver;
    int waitTime = 0;
    public static boolean isUserLogged = false;

    public SecretQuestionPage(WebDriver driver) {
            this.driver = driver;
            PageFactory.initElements(new AjaxElementLocatorFactory(driver, waitTime), this);
        }
    }

