package com.e2e.pages.main;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class UserPage extends BasePage {

    WebDriver driver;
    int waitTime = 0;

    @FindBy(xpath = "//a[@class='btn btn-primary']")
    private WebElement loc_btnCreateUser;

    public UserPage (WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver,waitTime ),this);


    }

    public UserCreationPage clickCreateUser() {
        loc_btnCreateUser.click();
        return new UserCreationPage(driver);
    }


}
