package com.e2e.pages.main;


import com.e2e.utilities.FrontActionsUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;


public class UserCreationPage extends BasePage {

    WebDriver driver;
    int waitTime = 10;
    Select select;
    WebDriverWait wait;


    @FindBy(id = "FirstName")
    private WebElement loc_inpFirstName;

    @FindBy(id = "LastName")
    private WebElement loc_inpLastName;

    @FindBy(id = "Password")
    private WebElement loc_inpPassword;

    @FindBy(id = "RepeatPassword")
    private WebElement loc_inpRepeatPassword;

    @FindBy(id = "Username")
    private WebElement loc_inpUsername;

    @FindBy(id = "NativeFullName")
    private WebElement loc_inpNativeFullName;

    @FindBy(id = "CountryID")
    private WebElement loc_ddCountry;

    @FindBy(id = "ddlCities")
    private WebElement loc_ddCity;

    @FindBy(id = "ddlLocations")
    private WebElement loc_ddLocation;

    @FindBy(id = "ddlLocationUnits")
    private WebElement loc_ddLocationUnit;

    @FindBy(id = "Gender")
    private WebElement loc_ddGender;

    @FindBy(id = "Email")
    private WebElement loc_inpEmail;

    @FindBy(css = "a[href='#tab-bi']")
    private WebElement loc_tabBusinessInformation;

    @FindBy(id = "CompanyID")
    private WebElement loc_ddCompany;

    @FindBy(id = "PositionID")
    private WebElement loc_ddPosition;

    @FindBy(id = "DepartmentID")
    private WebElement loc_ddDepartment;

    @FindBy(id = "ManagerID")
    private WebElement loc_ddReportingTo;

    @FindBy(id = "-WorkCategory")
    private WebElement loc_ddWorkCategory;

    @FindBy(id = "Designation")
    private WebElement loc_ddDesignation;

    @FindBy(id = "OccupationalClassificationID")
    private WebElement loc_ddOccupationalClassification;

    @FindBy(id = "DateOfCreation")
    private WebElement loc_dpStartDate;


    public UserCreationPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, waitTime), this);
    }


    public void fillUsername(String username) {
        loc_inpUsername.sendKeys(username);
    }

    public void fillFirstName(String firstName) {
        loc_inpFirstName.sendKeys(firstName);
    }

    public void fillLastName(String lastName) {
        loc_inpLastName.sendKeys(lastName);
    }

    public void fillPassword(String password) {
        loc_inpPassword.sendKeys(password);
    }

    public void fillRepeatPassword(String repeatPassword) {
        loc_inpRepeatPassword.sendKeys(repeatPassword);
    }

    public void fillNativeFullName(String nativeFullName) {
        loc_inpNativeFullName.sendKeys(nativeFullName);
    }

    public void fillEmail(String email) {
        loc_inpEmail.sendKeys(email);
    }

    public void fillMandatoryFieldsOnPersonalInformation(String username, String firstName,
                                                         String lastName, String password,
                                                         String repeatPassword, String nativeFullName,
                                                         String email) {
        fillUsername(username);
        fillFirstName(firstName);
        fillLastName(lastName);
        fillPassword(password);
        fillRepeatPassword(repeatPassword);
        fillNativeFullName(nativeFullName);
        fillEmail(email);
    }


    public void selectCountry(String country) {
        selectOptionFromDropdown(loc_ddCountry, country);
    }

    public void selectCity(String city) {
        selectOptionFromDropdown(loc_ddCity, city);
    }


    public void selectLocation(String location) {
        selectOptionFromDropdown(loc_ddLocation, location);
    }

    public void selectLocationUnit(String locationUnit) {
        selectOptionFromDropdown(loc_ddLocationUnit, locationUnit);
    }

    public void selectGender(String gender) {
        selectOptionFromDropdown(loc_ddGender, gender);
    }

    public void clickSecondTab() {
        FrontActionsUtil.move2ElementAndClick(driver, loc_tabBusinessInformation);
    }

    public void selectCompany(String company) {
        selectOptionFromDropdown(loc_ddCompany, company);
    }

    public void selectPosition(String position) {
        selectOptionFromDropdown(loc_ddPosition, position);
    }

    public void selectDepartment(String department) {
        selectOptionFromDropdown(loc_ddDepartment, department);
    }

    public void selectReportingTo(String reportingTo) {
        selectOptionFromDropdown(loc_ddReportingTo, reportingTo);
    }

    public void selectWorkCategory(String workCategory) {
        selectOptionFromDropdown(loc_ddWorkCategory, workCategory);
    }

    public void selectDesignation(String designation) {
        selectOptionFromDropdown(loc_ddDesignation, designation);
    }

    public void selectOccupationalClassification(String occupationalClassification) {
        loc_ddOccupationalClassification.sendKeys("2513 6002");
        selectOptionFromDropdown(loc_ddOccupationalClassification, occupationalClassification);
    }

    public void selectAllDropdownsFromPersonalInformation(String country, String city,
                                                          String location, String locationUnit, String gender) {
        selectCountry(country);
        selectCity(city);
        selectLocation(location);
        selectLocationUnit(locationUnit);
        selectGender(gender);
    }

    public void selectAllDropdownsFromBusinessInformation(String company, String position, String department,
                                                          String reportingTo, String workCategory, String designation,
                                                          String occupationalClassification) {
        selectCompany(company);
        selectPosition(position);
        selectDepartment(department);
        selectReportingTo(reportingTo);
        selectWorkCategory(workCategory);
        selectDesignation(designation);
        selectOccupationalClassification(occupationalClassification);
    }

}










