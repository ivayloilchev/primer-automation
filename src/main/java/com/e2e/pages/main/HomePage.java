package com.e2e.pages.main;

import com.e2e.config.ConfigurationManager;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class HomePage extends BasePage {

    static WebDriver driver;
    int waitTime = 0;
    static String baseUrl = ConfigurationManager.configuration().baseUrl();


    @FindBy (id = "navbarDropdownMenuLinkNotifications")
    private WebElement loc_ddProfileMenu;

    @FindBy (css="a[href='/NewsFeeds/List/']")
    private static WebElement loc_btnNewsList;

    public HomePage(WebDriver driver){
        super(driver);
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver,waitTime),this);

    }

    public boolean isProfileMenuDisplayed() {
        try {
            return loc_ddProfileMenu.isDisplayed();
        }

        catch (NoSuchElementException e) {
            return false;
        }
    }

    public static boolean isNewsListDisplayed() {
        try {
            return loc_btnNewsList.isDisplayed();
        }
        catch (NoSuchElementException e) {
            return false;
        }
    }

    public static HomePage navigateToHomePage() {
        driver.get(baseUrl);
        return new HomePage(driver);
    }




}


