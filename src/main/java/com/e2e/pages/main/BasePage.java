package com.e2e.pages.main;

import com.e2e.utilities.FrontActionsUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class BasePage {


    WebDriver driver;
    int waitTime = 10;
    Select select;
    WebDriverWait wait;

    @FindBy (css = "a[href='/Users/ListUsers']")
    private WebElement loc_btnUsers;


    public BasePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, waitTime), this);
        wait = new WebDriverWait(this.driver, Duration.ofSeconds(waitTime));
    }


    public UserPage clickUserPage(){
        loc_btnUsers.click();
        return new UserPage(driver);
    }

    public void selectOptionFromDropdown(WebElement element, String option){
        select = new Select(element);
        wait.until(ExpectedConditions.elementToBeClickable(element));
        select.selectByVisibleText(option);
    }
}





