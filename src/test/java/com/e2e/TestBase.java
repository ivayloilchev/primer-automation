package com.e2e;

import com.e2e.config.Configuration;
import com.e2e.config.ConfigurationManager;
import com.e2e.driver.DriverManager;
import com.e2e.pages.authentication.LoginPagePrimer;
import com.e2e.pages.main.HomePage;
import jdk.internal.net.http.common.Log;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Arrays;


public abstract class TestBase {

    String baseUrl = ConfigurationManager.configuration().baseUrl();
    String baseUsername = ConfigurationManager.configuration().baseUsername();
    String basePassword = ConfigurationManager.configuration().basePassword();
    HomePage homePage;

    public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd.HHmmss");

    protected static final boolean DEMO = true;
    protected static int demoWait = 2000;
    protected String url = "";

    //protected String reportFolder = System.getProperty("user.dir") + "\\test-output\\SmokeSuite\\";
    private final String urlReport = System.getProperty("user.dir") + "\\test-output\\SmokeSuite\\" + "SmokeTests.html";
    boolean autoShowReport = ConfigurationManager.configuration().showReport();

    protected static WebDriver driver;
    protected static WebDriverWait wait;
    public int waitTime = 0;

    public static boolean dockingAllowed = ConfigurationManager.configuration().dockingAllowed();
    public static String dockSide = ConfigurationManager.configuration().dockSide();


    @BeforeSuite(alwaysRun = true)
    public void suiteSetup() {

        String browserName = ConfigurationManager.configuration().browser();
        driver = DriverManager.createInstance(browserName);
        driver.manage().window().maximize();

        wait = new WebDriverWait(driver, Duration.ofSeconds(ConfigurationManager.configuration().waitTime()));
        waitTime = ConfigurationManager.configuration().waitTime();

        if (dockingAllowed) DriverManager.dockBrowserWindow(dockSide);

    }

    @BeforeTest(alwaysRun = true)
    public void testSetup() {
        driver.get(baseUrl);
    }

    @AfterSuite(alwaysRun = true)
    public void suiteTearDown() {
        if (driver != null) {
            driver.quit();
        }
        if (autoShowReport) {
            File htmlFile = new File(urlReport);
            try {
                Desktop.getDesktop().browse(htmlFile.toURI());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    @AfterMethod(alwaysRun = true)
    public void methodPostConditions(ITestResult testResult) throws IOException {

        if (testResult.getStatus() == ITestResult.FAILURE) {
            takeScreenshot(testResult.getName(), Arrays.toString(testResult.getParameters()));
        }

    }

    @BeforeClass
    public void generalPreconditions(){
        preconditions();

    }

    public abstract void preconditions();

    public static void takeScreenshot(String testMethod, String testParams) {
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile, new File(ConfigurationManager.configuration().path2Screenshots() + testMethod + "-"
                    + testParams + sdf.format(new Timestamp(System.currentTimeMillis())) + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void loginAsDefaultUserIfNotLogged() {
        if(!LoginPagePrimer.isUserLogged) {
            String url = "http://localhost:51258/Account/Login";
            LoginPagePrimer loginPagePrimer = new LoginPagePrimer(driver);
            driver.get(url);
            HomePage homePage = loginPagePrimer.setFormAndLogin(baseUsername, basePassword);
            if(!HomePage.isNewsListDisplayed()) {
                homePage = HomePage.navigateToHomePage();
            }
            LoginPagePrimer.isUserLogged = homePage.isProfileMenuDisplayed();
        }

    }

//    protected HomePage loginAsDefaultUserIfNotLogged() {
//        if(!LoginPagePrimer.isUserLogged) {
//            String url = "http://localhost:51258/Account/Login";
//            LoginPagePrimer loginPagePrimer = new LoginPagePrimer(driver);
//            driver.get(url);
//            HomePage homePage = loginPagePrimer.setFormAndLogin(baseUsername, basePassword);
//            if(!HomePage.isNewsListDisplayed()) {
//                homePage = HomePage.navigateToHomePage();
//            }
//            LoginPagePrimer.isUserLogged = homePage.isProfileMenuDisplayed();
//        }
//
//        return new HomePage(driver);
//    }


}
