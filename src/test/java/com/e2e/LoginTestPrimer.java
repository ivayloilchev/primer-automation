package com.e2e;

import com.e2e.pages.main.HomePage;
import com.e2e.pages.authentication.LoginPagePrimer;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LoginTestPrimer extends TestBase {

    LoginPagePrimer loginPage;
    HomePage homePage;
    String loginUrl = baseUrl + "Account/Login";

    @Override
    public void preconditions() {
    }

        @Test(priority = 1,
                enabled = true,
                dataProvider = "invalidLoginData",
                description = "Unsuccessful login with multiple invalid credentials")
                public void invalidLogin(String username, String password) {
            loginPage = new LoginPagePrimer(driver);
            driver.get(loginUrl);
            loginPage.setInvalidFormAndLogin(username,password);
            Assert.assertTrue(loginPage.getLoc_inputUsername().isDisplayed());

        }

        @Test(priority = 2,
            enabled = true,
            description = "Successful basic login with valid credentials")
        public void login() {
        loginPage = new LoginPagePrimer(driver);
        driver.get(loginUrl);
        homePage = loginPage.setFormAndLogin(baseUsername, basePassword);
        Assert.assertTrue(homePage.isProfileMenuDisplayed(), "Profile menu is not displayed!");


    }





        @DataProvider(name = "invalidLoginData")
        public Object[][] invalidLoginData() {
            Object[][] data = {
                    {"", "Demo!1234"},
                    {"trainer@example.com", ""},
                    {"", ""},
                    {"mnogobukvismqtamdanapishatuksigurnoshtesadalgi", "parolka"},
                    {"HRAdministrator ", "Qwerty1234 " }
            };
            return data;
        }


}
