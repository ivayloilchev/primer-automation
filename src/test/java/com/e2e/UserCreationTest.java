package com.e2e;

import com.e2e.pages.main.HomePage;
import com.e2e.pages.main.UserCreationPage;
import com.e2e.pages.main.UserPage;
import com.e2e.utilities.FrontActionsUtil;
import org.testng.annotations.Test;

public class UserCreationTest extends TestBase {

    UserPage userPage;
    HomePage homePage;
    UserCreationPage userCreationPage;
    String userPageUrl = baseUrl + "/Users/ListUsers";


    @Override
    public void preconditions() {
        loginAsDefaultUserIfNotLogged();
        userPage = new UserPage(driver);
    }



    @Test(priority = 1,
            enabled = true,
            description = "Successful user creation")
    public void createNewUser() throws InterruptedException {
        driver.get(userPageUrl);
        userCreationPage = userPage.clickCreateUser();
        userCreationPage.fillMandatoryFieldsOnPersonalInformation("ivaylo.tsucherov",
                "Ivaylo",
                "Tsucherov",
                "Qwerty123",
                "Qwerty123",
                "Ivaylo Tsucherov",
                "ivaylotsucherov@primeholding.com");
        userCreationPage.selectAllDropdownsFromPersonalInformation("Bulgaria",
                "Sofia",
                "Main Street 189",
              "3 Floor",
                "Male");
        userCreationPage.clickSecondTab();
        userCreationPage.selectAllDropdownsFromBusinessInformation("БмпСистемс",
                "QA",
                "QA",
                "Anton Antonov",
                "Direct",
                "Internal Direct",
                "Програмист, уеб сайтове (2513 6002)");
        Thread.sleep(5000);

    }
}
