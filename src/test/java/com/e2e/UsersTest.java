package com.e2e;

import com.e2e.pages.authentication.LoginPagePrimer;
import com.e2e.pages.main.BasePage;
import com.e2e.pages.main.HomePage;
import com.e2e.pages.main.UserPage;
import org.testng.annotations.Test;

public class UsersTest extends TestBase {

    LoginPagePrimer loginPage;
    BasePage basePage;
    HomePage homePage;
    UserPage userPage;
    String loginUrl = baseUrl + "Account/Login";


    @Override
    public void preconditions() {
        loginAsDefaultUserIfNotLogged();
        userPage = new UserPage(driver);
        basePage = new BasePage(driver);
    }

    @Test(priority = 1,
            enabled = true,
            description = "Successful basic login with valid credentials")
    public void createUser() throws InterruptedException {
        userPage = basePage.clickUserPage();
        userPage.clickCreateUser();

    }


}
